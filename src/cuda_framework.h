#ifndef CUDA_FRAMEWORK_H
#define CUDA_FRAMEWORK_H

#ifdef CUDA_SRC_FILE
    #define EXTERN
#else
    #define EXTERN extern
#endif

#include <dynlink/cuda_runtime_api_dynlink.h>

void        cuda_initialize(int argc, char **argv);

EXTERN bool        use_cuda;
EXTERN CUdevice    device;

EXTERN int         max_freq;

EXTERN int         ATTRIBUTE_MAX_BLOCK_DIM_X;
EXTERN int         ATTRIBUTE_MAX_BLOCK_DIM_Y;
EXTERN int         ATTRIBUTE_MAX_BLOCK_DIM_Z;
EXTERN int         ATTRIBUTE_MAX_GRID_DIM_X;
EXTERN int         ATTRIBUTE_MAX_GRID_DIM_Y;
EXTERN int         ATTRIBUTE_MAX_GRID_DIM_Z;

EXTERN int         COMPUTE_MAJOR, COMPUTE_MINOR;

#endif
