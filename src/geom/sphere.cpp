/**
 * @file sphere.cpp
 * @brief Function defnitions for the Sphere class.
 *
 * @author Kristin Siu (kasiu)
 * @author Eric Butler (edbutler)
 */

/*
    EDIT THIS FILE FOR P1. However, do not change existing constructor
    signatures. The staff scene loader requires all of those to be intact.
 */

#include "462math.h"
#include "geom/sphere.h"
#include "glheaders.h"
#include <cmath>
#include <cstdio>

#if 0
Sphere::Sphere()
    : radius(0) {}

Sphere::Sphere(const Vec3& pos, const Quat& ori, const Vec3& scl,
               real_t rad, Material* mat, Effect* efc)
    : Geometry(pos, ori, scl, mat, efc), radius(rad)
{
    inverse_orientation = orientation.inverse();
}

Sphere::~Sphere() {}

void Sphere::draw() const
{
    // TODO P1 draw the geometry in local space
}
#endif

float Sphere::intersects(const Ray &ray, const Geometry *geom)
{
    const SphereData *data = (const SphereData *) geom->geom_specific;
    Ray transformed(ray);
    transformed.origin    -= geom->position;

    transformed.origin /=    geom->scale;
    transformed.direction /= geom->scale;

    transformed.origin *= geom->inverse_orientation;
    transformed.direction *= geom->inverse_orientation;

    Vec3 e_c = transformed.origin;
    real_t d_dot_d = transformed.direction.dot(transformed.direction);
    real_t disc = pow(transformed.direction.dot(e_c), 2.0);
    disc -= d_dot_d * (e_c.dot(e_c) - pow(data->radius, 2.0));
    if (disc < 0)
        return -1;
    real_t num = - transformed.direction.dot(e_c) - sqrt(disc);
    return num / d_dot_d;
}

void Sphere::get_normal(const Vec3 &pt, const Geometry *geom, Vec3 &out) 
{
    out =  pt;
    out -= geom->position;
    out /= geom->scale;
    out *= geom->inverse_orientation;
}
