/** @file raytrace.cpp
 *  @brief The primary raytracing routines.
 *
 *  @author Eric Butler (edbutler)
 */

#include "462math.h"
#include "project.h"
#include "scene.h"
#include "vec/vec.h"
#include <iostream>
#include <cmath>

#define RT_NUM_ROWS_PER_UPDATE 20
#define EPSILON 0.001
#define N_BOUNCES 1

Vec3 raytrace(const Scene *scene, const Ray &ray, int counter);

/**
 * Traces a certain pixel.
 * @param scene The scene to trace.
 * @param x The pixel index in the x direction.
 * @param y The pixel index in the y direction.
 * @param width The total width in pixels.
 * @param height The total height in pixels.
 * @return The color of the given pixel.
 */
static Vec3 rt_trace_pixel(Scene* scene, int x, int y,
                           int width, int height)
{
    real_t fov = scene->camera.fov;
    real_t xfov = fov * width / height;
    real_t x_ang = - xfov / 2.0 + xfov / width * x;
    real_t y_ang = - fov / 2.0 + fov / height * y;

    Ray ray;
    Quat inverse_cam_transform = scene->camera.orientation.inverse();
    ray.origin = scene->camera.position;
    ray.direction.x = sin(x_ang);
    ray.direction.y = sin(y_ang);
    ray.direction.z = -1;

    ray.direction = inverse_cam_transform * ray.direction;

    return raytrace(scene, ray, N_BOUNCES);
}

Vec3 raytrace(const Scene *scene, const Ray &ray, int counter)
{
    Geometry *closest;
    real_t   min_t;
    closest = scene->nearest_intersection(ray, min_t);

    if (closest == NULL)
        return scene->ambient_light;

    Vec3 intersection = ray.origin + min_t * ray.direction;
    Vec3 normal;
    closest->get_normal(intersection, normal);
    normal.normalize();

    Material *curr_material = closest->material;

    intersection += EPSILON * normal;

    Scene::LightList light_lis;
    scene->light_intersections(intersection, normal, light_lis);

    Vec3   ret_color(0, 0, 0);

    ret_color += scene->ambient_light * closest->material->ambient_reflec;

    for (Scene::LightList::iterator itr = light_lis.begin();
            itr != light_lis.end();
            ++itr)
    {
        Light &curr_light = *itr;
        Vec3 to_add(0, 0, 0);

        Vec3 dir = curr_light.position - intersection;
        dir.normalize();

        Vec3 reflec_dir = - dir + 2 * normal;
        reflec_dir.normalize();

        Vec3 to_eye = scene->camera.position - intersection;
        to_eye.normalize();

        to_add += curr_material->diffuse_reflec *
            dir.dot(normal) * curr_light.color;

        to_add += curr_material->spec_reflec *
            pow(reflec_dir.dot(to_eye), curr_material->shininess) *
            curr_light.color;

        to_add += curr_material->diffuse * dir.dot(normal);

        ret_color += to_add;
    }
    
    if (counter <= 0)
        return ret_color;

    Vec3 new_direction = ray.direction;
    new_direction.normalize();
    new_direction += 2 * normal;

    Ray new_ray;
    new_ray.origin = intersection;
    new_ray.direction = new_direction;

    Vec3 bounced_color = raytrace(scene, new_ray, counter - 1);

    ret_color += curr_material->ambient_reflec * bounced_color;

    return ret_color;
}


/**
 * Struct associated with rt_raytrace.
 */
struct RaytraceState {
    int row;

    // TODO add additional members you need for rt_raytrace.

    RaytraceState()
        : row(0) {}
};

/**
 * The current raytrace state.
 */
static RaytraceState raytrace_state;

/**
 * Raytrace part of the current scene. This function is invoked repeatedly
 * during a raytrace until it signifies that it is complete by returning true.
 * As much work as would like to be displayed in the next render should be
 * done in a single call.
 * @param scene The scene to raytrace.
 * @param width Width of the framebuffer.
 * @param height Height of the framebuffer.
 * @param buffer The buffer into which to place the color data. It is
 *  32-bit RGBA (4 bytes per pixel), in row-major order.
 * @param new_trace Set to true on the first call of a new raytrace. This may
 *  be set to true even before a previous ray trace is completed. If this is
 *  the case, then the previous raytrace is aborted and will never be finished.
 * @return False if there is more work to be done on this raytrace, true if
 *  it has been completed.
 * @remark If new_trace is false, all other parameters are guaranteed to be
 *  identical as the last invocation of this function.
 */
bool rt_raytrace(Scene* scene, int width, int height,
                 unsigned char *buffer, bool new_trace)
{
    // if a new trace, reset our state
    if (new_trace) {
        std::cout << "Beginning raytrace....\n";
        raytrace_state = RaytraceState();
        // TODO do any precomputations before trace begins
    }

    // get the color of each pixel, some number of rows at a time
    int row = raytrace_state.row;
    for (int i = 0; i < RT_NUM_ROWS_PER_UPDATE && row < height; i++, row++) {
        for (int j = 0; j < width; j++) {
            color_vector_to_array(
                Vec4(rt_trace_pixel(scene, j, row, width, height), 1.0),
                buffer + 4 * (row * width + j));
        }
    }
    raytrace_state.row = row;

    // stop once we've traced every row
    std::cout << "traced through row " << row << '\n';
    return row == height;
}

