#define CUDA_SRC_FILE
#include <iostream>
#include "cuda_framework.h"

using namespace std;

void cuda_initialize(int argc, char **argv)
{
    int n_devices;

    if (cuDeviceGetCount(&n_devices) != CUDA_SUCCESS)
    {
        cout << "Failed to get number of CUDA devices." << endl;
        return;
    }

    if (n_devices == 0)
    {
        cout << "No CUDA-capable devices found." << endl;
        return;
    }

    int max_khz = -1;
    CUdevice tmp_device;
    for (int curr_device_ordinal = 0; curr_device_ordinal < n_devices; curr_device_ordinal++)
    {
        if (cuDeviceGet(&tmp_device, curr_device_ordinal) != CUDA_SUCCESS)
        {
            cout << "Failed to get information about device " << curr_device_ordinal << endl;
            continue;
        }
        int curr_khz;
        if (cuDeviceGetAttribute(&curr_khz, CU_DEVICE_ATTRIBUTE_CLOCK_RATE, tmp_device) != 
                CUDA_SUCCESS)
        {
            cout << "Failed to get clock speed information about device " << curr_device_ordinal << endl;
            continue;
        }
        if (curr_khz > max_khz)
        {
            max_khz = curr_khz;
            device = tmp_device;
        }
    }

    if (cuDeviceGetAttribute(&ATTRIBUTE_MAX_BLOCK_DIM_X,
                CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_X,
                device) != CUDA_SUCCESS)
    {
        cout << "Failed to get CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_X" << endl;
        return;
    }
    if (cuDeviceGetAttribute(&ATTRIBUTE_MAX_BLOCK_DIM_Y,
                CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Y,
                device) != CUDA_SUCCESS)
    {
        cout << "Failed to get CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Y" << endl;
        return;
    }
    if (cuDeviceGetAttribute(&ATTRIBUTE_MAX_BLOCK_DIM_Z,
                CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Z,
                device) != CUDA_SUCCESS)
    {
        cout << "Failed to get CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Z" << endl;
        return;
    }
    if (cuDeviceGetAttribute(&ATTRIBUTE_MAX_GRID_DIM_X,
                CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_X,
                device) != CUDA_SUCCESS)
    {
        cout << "Failed to get CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_X" << endl;
        return;
    }
    if (cuDeviceGetAttribute(&ATTRIBUTE_MAX_GRID_DIM_Y,
                CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_Y,
                device) != CUDA_SUCCESS)
    {
        cout << "Failed to get CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_Y" << endl;
        return;
    }
    if (cuDeviceGetAttribute(&ATTRIBUTE_MAX_GRID_DIM_Z,
                CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_Z,
                device) != CUDA_SUCCESS)
    {
        cout << "Failed to get CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_Z" << endl;
        return;
    }

    if (max_khz == -1)
    {
        cout << "Failed to get a suitable device." << endl;
        return;
    }

    if (cuDeviceComputeCapability(&COMPUTE_MAJOR, &COMPUTE_MINOR, device)
            != CUDA_SUCCESS)
    {
        cout << "Failed to get compute capability" << endl;
        return;
    }

    if (COMPUTE_MAJOR < 1 && COMPUTE_MINOR < 1)
    {
        cout << "Insufficiently high compute capability " << COMPUTE_MAJOR << "." <<
            COMPUTE_MINOR << " on fastest device." << endl;
        return;
    }

    max_freq = max_khz;
    use_cuda = true;

    cout << "Using CUDA device with clock speed " << max_khz << "khz and compute capability "
        << COMPUTE_MAJOR << "." << COMPUTE_MINOR << endl;
}
