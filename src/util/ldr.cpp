/**
 * @file ldr.cpp
 * @brief contains main scene loading function and student-created scenes.
 *
 * @author Eric Butler (edbutler)
 */

/*
OPTIONAL: add your own scenes to this file.
*/

#include <iostream>
#include "project.h"
#include "scene.h"
#include "geom/sphere.h"
#include "geom/triangle.h"
#include "geom/watersurface.h"

static void ldr_load_example_scene(Scene* scene)
{
    Camera& cam = scene->camera;
    cam.orientation = Quat::Identity;
    cam.position = Vec3(0,0,10);
    cam.focus_dist = 10;
    cam.fov = PI / 3.0;
    cam.near_clip = .1;
    cam.far_clip = 100.0;

    scene->ambient_light = Vec3(.1,.1,.1);

    Material* mat1, *mat2;
    mat1 = scene->alloc_material();
    mat1->ambient = Vec3(0,0,1);
    mat1->diffuse = Vec3(0,0,1);
    mat1->phong = Vec3(.3,.3,1);
    mat1->shininess = 20;
    mat1->spec_reflec = 0.5;
    mat1->diffuse_reflec = 0.5;
    mat1->ambient_reflec = 0.7;

    mat2 = scene->alloc_material();
    mat2->ambient = Vec3(0,0,1);
    mat2->diffuse = Vec3(0,0,1);
    mat2->phong = Vec3(.3,.3,1);
    mat2->shininess = 20;
    mat2->spec_reflec = 0.5;
    mat2->diffuse_reflec = 0.5;
    mat2->ambient_reflec = 0.7;

    Geometry *obj1, *obj2;
    obj1 = scene->alloc_geom();
    obj2 = scene->alloc_geom();

    obj1->geom_specific = malloc(sizeof(SphereData));
    obj2->geom_specific = malloc(sizeof(SphereData));

    obj1->position = Vec3(2, 0, 3);
    obj2->position = Vec3(0, 0, 0);

    obj1->scale = Vec3(1, 1, 1);
    obj2->scale = Vec3(1, 1, 1);

    SphereData *data1 = (SphereData *) obj1->geom_specific;
    SphereData *data2 = (SphereData *) obj2->geom_specific;

    data1->radius = 0.5;
    data2->radius = 0.5;

    obj1->material = mat1;
    obj2->material = mat2;

    Light *light1, *light2;
    light1 = scene->alloc_light();
    light2 = scene->alloc_light();

    light1->position = Vec3(0, 0, 5);
    light1->color = Vec3::Ones;

    light2->position = Vec3(1, 0, -2);
    light2->color = Vec3::Ones;
}

/**
 * Loads the scene with the given num into scene.
 * @param scene The Scene into which to load.
 * @param num The id of the scene to load.
 * @return true on successful load, false otherwise.
 */
bool ldr_load_scene(Scene* scene, int num)
{
    // TODO OPTIONAL add function calls to load your own scenes here.
    // scenes [0, MAX_STAFF_SCENE_NUM) are reserved for staff.
    switch (num)
    {
        case 10:
            std::cout << "Loaded example scene" << std::endl;
            ldr_load_example_scene(scene);
            break;
        default:
            return false;
    }

    return true;
}

