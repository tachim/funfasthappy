/**
 * @file staffldr.cpp
 * @brief Contans the load functions for the staff's built-in test scenes.
 *
 * @author Eric Butler (edbutler)
 * @author Zeyang Li (zeyangl)
 */

/*
    DO NOT MODIFY THIS FILE. It contains the staff test scnes and will be
    updated with each project handout. If you want to add your own scenes,
    do so elsewhere. The suggested file for that is ldr.cpp.
 */

#include "project.h"
#include "scene.h"
#include "geom/sphere.h"
#include "geom/triangle.h"
#include "geom/watersurface.h"
#include <iostream>

static void create_square(Scene* scene, const Vec3& xaxis, const Vec3& yaxis,
                          const Vec3& corner, const Vec3& normal, Material* mat,
                          const Vec2& tcoord_min, const Vec2& tcoord_unit, Effect* effect)
{
    Vec3 maxi = corner + xaxis + yaxis;

    Vec3 vertices[3];
    Vec3 normals[] = {normal, normal, normal};
    Material* materials[] = { mat, mat, mat };
    Vec2 tcoords[3];

#ifdef UV_CLOCKWISE
    Vec2 dist(xaxis.magnitude(), yaxis.magnitude());	
#else
	// computing uvs in counterclockwise order
	Vec2 dist(yaxis.magnitude(), xaxis.magnitude());
#endif

    vertices[0] = maxi;
    vertices[1] = corner;
    vertices[2] = corner + yaxis;
	
    tcoords[0] = tcoord_min + dist * tcoord_unit;
    tcoords[1] = tcoord_min;
#ifdef UV_CLOCKWISE
    tcoords[2] = tcoord_min + Vec2(0, dist.y) * tcoord_unit;
#else
	tcoords[2] = tcoord_min + Vec2(dist.x, 0) * tcoord_unit;
#endif

    scene->objects.push_back(
        new Triangle(Vec3::Zero, Quat::Identity, Vec3::Ones,
                     vertices, tcoords, normals, materials, effect));

    vertices[0] = corner;
    vertices[1] = maxi;
    vertices[2] = corner + xaxis;
	
    tcoords[0] = tcoord_min;
    tcoords[1] = tcoord_min + dist * tcoord_unit;
#ifdef UV_CLOCKWISE
    tcoords[2] = tcoord_min + Vec2(dist.x, 0) * tcoord_unit;
#else
	tcoords[2] = tcoord_min + Vec2(0, dist.y) * tcoord_unit;
#endif

    scene->objects.push_back(
        new Triangle(Vec3::Zero, Quat::Identity, Vec3::Ones,
                     vertices, tcoords, normals, materials, effect));

}

#define SIZE 4

static void ldr_load_scene00(Scene* scene)
{
    //"Cornell Box" Scene
    Camera& cam = scene->camera;
    cam.orientation = Quat::Identity;
    cam.position = Vec3(0,2,9.2);
    cam.focus_dist = sqrt(4+9.2*9.2);
    cam.fov = PI / 3.0;
    cam.near_clip = .1;
    cam.far_clip = 100.0;

    scene->ambient_light = Vec3(.1,.1,.1);
    scene->refraction_index = 1;
    scene->caustic_generator = 0;

    Vec3 floor_corner = Vec3(-SIZE, -SIZE/2, -SIZE);
    Vec3 ceiling_corner = Vec3(SIZE, SIZE + SIZE/2, -SIZE);

    //Green
    Material* g_mat = new Material();
    g_mat->ambient = Vec3(0.08, 0.57, 0.09);
    g_mat->diffuse = Vec3(0.08, 0.57, 0.09); 
    g_mat->phong = Vec3::Zero;
    g_mat->shininess = 40;
    g_mat->specular = Vec3::Zero;
    g_mat->refraction_index = 0;
    scene->materials.push_back(g_mat);

    create_square(scene,
        Vec3(0,0,SIZE-ceiling_corner.z), Vec3(0, floor_corner.y - ceiling_corner.y,0), 
        ceiling_corner, Vec3::UnitX * -1, g_mat, Vec2::Ones, Vec2::Zero, 0);

    //Red
    Material* r_mat = new Material();
    r_mat->ambient = Vec3(0.78, 0.04, 0.04);
    r_mat->diffuse = Vec3(0.78, 0.04, 0.04); 
    r_mat->specular = Vec3::Zero;
    r_mat->refraction_index = 0;
    scene->materials.push_back(r_mat);

    create_square(scene,
        Vec3(0,0,SIZE-ceiling_corner.z), Vec3(0, ceiling_corner.y - floor_corner.y,0), 
        floor_corner, Vec3::UnitX, r_mat, Vec2::Ones, Vec2::Zero, 0);

    //White
    Material* w_mat = new Material();
    w_mat->ambient = Vec3::Ones;
    w_mat->diffuse = Vec3::Ones; 
    w_mat->specular = Vec3::Zero;
    w_mat->refraction_index = 0;
    scene->materials.push_back(w_mat);

    create_square(scene,
        Vec3(0,0,SIZE-ceiling_corner.z), Vec3(ceiling_corner.x - floor_corner.x,0,0), 
        floor_corner, Vec3::UnitY, w_mat, Vec2::Ones, Vec2::Zero, 0);
    create_square(scene,
        Vec3(ceiling_corner.x - floor_corner.x,0,0), Vec3(0, ceiling_corner.y - floor_corner.y,0), 
        floor_corner, Vec3::UnitZ, w_mat, Vec2::Ones, Vec2::Zero, 0);
    create_square(scene,
        Vec3(0,0,SIZE-ceiling_corner.z), Vec3(floor_corner.x - ceiling_corner.x,0,0), 
        ceiling_corner, Vec3::UnitY * -1, w_mat, Vec2::Ones, Vec2::Zero, 0);

    //Crystal
    //Meant to have the properties of crystal
    Material* c_mat = new Material();
    c_mat->ambient = Vec3::Ones;
    c_mat->diffuse = Vec3::Ones;
    c_mat->phong = Vec3::Ones;
    c_mat->shininess = 10;
    c_mat->specular = Vec3::Ones;
    c_mat->refraction_index = 2;
    scene->materials.push_back(c_mat);

    scene->objects.push_back(
        new Sphere(Vec3(1, -1, 1), Quat::Identity, Vec3(1,1,1), 1, c_mat));

    //Mirror
    Material* m_mat = new Material();
    m_mat->ambient = Vec3::Ones;
    m_mat->diffuse = Vec3::Zero;
    m_mat->specular = Vec3::Ones;
    m_mat->refraction_index = 0;
    scene->materials.push_back(m_mat);

    scene->objects.push_back(
        new Sphere(Vec3(-1, 0, -1.75), Quat::Identity, Vec3(1,1,1), 2, m_mat));

    Light light;
    light.position = Vec3(0, 4, 0);
    light.color = Vec3(.7, .7, .7);
    scene->lights.push_back(light);
}

// pool boundaries
#define PIX 5
#define POX 9
#define PIZ 5
#define POZ 9
#define POY -.5
#define PIY -4
#define PBY -5

static void create_pool(Scene* scene)
{
    Material* mat = new Material();
    mat->ambient = Vec3(0.7, 0.7, 0.7);
    mat->diffuse = Vec3::Ones;
    mat->phong = Vec3::Ones;
    mat->shininess = 40;
    mat->specular = Vec3(.4,.4,.4);
    mat->refraction_index = 0;
	mat->texture_name = "images/marble.png";
    scene->materials.push_back(mat);

    Effect* efc = 0;

    Vec2 tcmin(0,0);
    Vec2 tcunit(.25,.25);

    // so very, very disgusting. please don't look, your eyes may melt

    // upper corners
    create_square(scene,
                  Vec3(POX-PIX,0,0), Vec3(0,0,2*POZ), Vec3(PIX,POY,-POZ),
                  Vec3::UnitY, mat, tcmin, tcunit, efc);
    create_square(scene,
                  Vec3(POX-PIX,0,0), Vec3(0,0,2*POZ), Vec3(-POX,POY,-POZ),
                  Vec3::UnitY, mat, tcmin, tcunit, efc);

    create_square(scene,
                  Vec3(2*PIX,0,0), Vec3(0,0,POZ-PIZ), Vec3(-PIX,POY,-POZ),
                  Vec3::UnitY, mat, tcmin, tcunit, efc);
    create_square(scene,
                  Vec3(2*PIX,0,0), Vec3(0,0,POZ-PIZ), Vec3(-PIX,POY,PIZ),
                  Vec3::UnitY, mat, tcmin, tcunit, efc);

    // inner sides

    create_square(scene,
                  Vec3(0,0,2*PIZ), Vec3(0,POY-PIY,0), Vec3(-PIX,PIY,-PIZ),
                  Vec3::UnitX, mat, tcmin, tcunit, efc);

    create_square(scene,
                  -Vec3(2*PIX,0,0), Vec3(0,POY-PIY,0), Vec3(PIX,PIY,-PIZ),
                  Vec3::UnitZ, mat, tcmin, tcunit, efc);

    create_square(scene,
                  -Vec3(0,POY-PIY,0), -Vec3(0,0,2*PIZ), Vec3(PIX,POY,PIZ),
                  -Vec3::UnitX, mat, tcmin, tcunit, efc);

    create_square(scene,
                  Vec3(2*PIX,0,0), Vec3(0,POY-PIY,0), Vec3(-PIX,PIY,PIZ),
                  -Vec3::UnitZ, mat, tcmin, tcunit, efc);

    // outer sides
    create_square(scene,
                  Vec3(0,POY-PBY,0), Vec3(0,0,2*POZ), Vec3(-POX,PBY,-POZ),
                  -Vec3::UnitX, mat, tcmin, tcunit, efc);

    create_square(scene,
                  Vec3(0,0,2*POZ), Vec3(0,POY-PBY,0), Vec3(POX,PBY,-POZ),
                  Vec3::UnitX, mat, tcmin, tcunit, efc);

    create_square(scene,
                  Vec3(2*POX,0,0), Vec3(0,POY-PBY,0), Vec3(-POX,PBY,-POZ),
                  -Vec3::UnitZ, mat, tcmin, tcunit, efc);

    create_square(scene,
                  Vec3(0,POY-PBY,0), Vec3(2*POX,0,0), Vec3(-POX,PBY,POZ),
                  Vec3::UnitZ, mat, tcmin, tcunit, efc);

    // bottom
    create_square(scene,
                  Vec3(0,0,2*PIZ), Vec3(2*PIX,0,0), Vec3(-PIX,PIY,-PIZ),
                  Vec3::UnitY, mat, tcmin, tcunit, efc);
    create_square(scene,
                  Vec3(0,0,2*POZ), Vec3(2*POX,0,0), Vec3(-POX,PBY,-POZ),
                  -Vec3::UnitY, mat, tcmin, tcunit, efc);

}

static void ldr_load_scene01(Scene* scene)
{
    // "Pool" Scene

    Camera& cam = scene->camera;
    cam.orientation = Quat(-0.0946664, -0.00690199, 0.970616, 0.22112);
    cam.position = Vec3(-2.62381,6.01017,-12.4194);
    cam.focus_dist = 14.0444;
    cam.fov = PI / 3.0;
    cam.near_clip = 1;
    cam.far_clip = 1000.0;

	// create sphere map
	SphereMap* spheremap = new SphereMap();
	scene->background = spheremap;
	spheremap->texture_name = "images/spheremap_stpeters.png";

    scene->ambient_light = Vec3(.2,.2,.2);
    scene->refraction_index = 1;

    create_pool(scene);

    Material* mat;

    mat = new Material();
    mat->ambient = Vec3(0.0, 0.2, 0.3);
    mat->diffuse = Vec3(0.0, 0.2, 0.3); // blue water
    mat->phong = Vec3::Ones;
    mat->shininess = 20;
    mat->specular = Vec3::Ones;
    mat->refraction_index = 1.33;
    scene->materials.push_back(mat);

    WaterSurface::WavePointList wave_points;
    WaterSurface::WavePoint* p;

    wave_points.push_back(WaterSurface::WavePoint());
    p = &wave_points.back();
    p->position = Vec2(.42,.56);
    p->falloff = 2;
    p->coefficient = .3;
    p->timerate = -6*PI;
    p->period = 16*PI;

    wave_points.push_back(WaterSurface::WavePoint());
    p = &wave_points.back();
    p->position = Vec2(-.58,-.30);
    p->falloff = 2;
    p->coefficient = .3;
    p->timerate = -8*PI;
    p->period = 20*PI;

    WaterSurface* water_surface = new WaterSurface(Vec3(0, POY - 1, 0),
                                     Quat::Identity,
                                     Vec3(PIX, 0.4, PIZ),
                                     wave_points,
                                     140, 140, mat);
    scene->objects.push_back(water_surface);
    scene->updatable_objects.push_back(water_surface);
    scene->caustic_generator = water_surface;

	Effect* fresnel = new FresnelEffect("shaders/fresnel_vert.glsl",
										"shaders/fresnel_frag.glsl",
										spheremap, mat);
	water_surface->effect = fresnel;
	scene->effects.push_back(fresnel);

    mat = new Material();
    mat->ambient = Vec3::Ones;
    mat->diffuse = Vec3::Ones;
    mat->phong = Vec3(1,.5,1);
    mat->shininess = 20;
    mat->specular = Vec3(.6,.6,.6);
    mat->refraction_index = 0;
    mat->texture_name = "images/swirly.png";
    scene->materials.push_back(mat);

    real_t rad = 2;
    scene->objects.push_back(
        new Sphere(Vec3((POX+PIX)/2, POY+rad, (POZ+PIZ)/2),
                   Quat::Identity, Vec3::Ones, rad, mat));
    scene->objects.push_back(
        new Sphere(Vec3(-(POX+PIX)/2, POY+rad, -(POZ+PIZ)/2),
                   Quat::Identity, Vec3::Ones, rad, mat));

    mat = new Material();
    mat->ambient = Vec3::Ones;
    mat->diffuse = Vec3::Ones;
    mat->phong = Vec3::Ones;
    mat->shininess = 100;
    mat->specular = Vec3::Ones;
    mat->refraction_index = 2;
    scene->materials.push_back(mat);

    scene->objects.push_back(
        new Sphere(Vec3(-(POX+PIX)/2, POY+rad, (POZ+PIZ)/2),
                   Quat::Identity, Vec3::Ones, rad, mat));
    scene->objects.push_back(
        new Sphere(Vec3((POX+PIX)/2, POY+rad, -(POZ+PIZ)/2),
                   Quat::Identity, Vec3::Ones, rad, mat));

    Light light;
    light.position = Vec3(-4, 8.5, 8) * 30;
    light.color = Vec3(.7,.7,.7);
    scene->lights.push_back(light);
}

static void ldr_load_scene02(Scene* scene)
{
    // tretrahedron scene

    Camera& cam = scene->camera;
    cam.orientation = Quat(-0.324955, -0.131551, 0.86558, 0.357589);
    cam.position = Vec3(-6.56632,5.33547,-5.33068);
    cam.focus_dist = 10;
    cam.fov = PI / 7.0;
    cam.near_clip = .1;
    cam.far_clip = 100.0;

	SphereMap* spheremap = new SphereMap();
	scene->background = spheremap;
	spheremap->texture_name = "images/spheremap_stpeters.png";

    scene->ambient_light = Vec3(.1,.1,.1);

    Light light;
    light.position = Vec3(4, 8.5, 8) * 30;
    light.color = Vec3(.7,.7,.7);
    scene->lights.push_back(light);

    light.position = Vec3(-2, .5, -4) * 30;
    light.color = Vec3(.5,.5,.5);
    scene->lights.push_back(light);

    Material* mat[4];
    for (int i=0; i<4; i++) {
        mat[i] = new Material();
        scene->materials.push_back(mat[i]);
    }

    mat[0]->ambient = Vec3(.8,0,0);
    mat[0]->diffuse = Vec3(.8,0,0);
    mat[0]->phong = Vec3(1,.2,.2);
    mat[0]->shininess = 100;
    mat[0]->specular = Vec3(.8,.2,.2);
    mat[0]->refraction_index = 0;

    mat[1]->ambient = Vec3(.1,.1,.9);
    mat[1]->diffuse = Vec3(.1,.1,.9);
    mat[1]->phong = Vec3(.1,.1,.2);
    mat[1]->shininess = 3;
    mat[1]->specular = Vec3(.1,.1,.2);
    mat[1]->refraction_index = 0;

    mat[2]->ambient = Vec3::Ones;
    mat[2]->diffuse = Vec3::Ones;
    mat[2]->phong = Vec3::Ones;
    mat[2]->shininess = 100;
    mat[2]->specular = Vec3(.7,.2,.7);
    mat[2]->refraction_index = 0;
    mat[2]->texture_name = "images/swirly.png";

    mat[3]->ambient = Vec3::Zero;
    mat[3]->diffuse = Vec3::Zero;
    mat[3]->phong = Vec3::Ones;
    mat[3]->shininess = 300;
    mat[3]->specular = Vec3::Ones;
    mat[3]->refraction_index = 0;

    Vec3 vert[] = { Vec3(1,1,1), Vec3(-1,-1,1), Vec3(-1,1,-1), Vec3(1,-1,-1) };
    Vec2 tc[] = { Vec2(0,0), Vec2(.5,1), Vec2(.5,.5), Vec2(1,0) };

    {
        Vec3 vertices[] = { vert[0], vert[1], vert[2] };
        Vec3 norm = (vertices[2]-vertices[0]).cross(vertices[1]-vertices[0]);
        norm.normalize();
        Vec3 normals[] = { norm, norm, norm };
        Vec2 tcoords[] = { tc[0], tc[1], tc[2] };
        Material* materials[] = { mat[0], mat[1], mat[2] };

        scene->objects.push_back(
            new Triangle(Vec3::Zero, Quat::Identity, Vec3::Ones,
                         vertices, tcoords, normals, materials, 0));
    }


    {
        Vec3 vertices[] = { vert[1], vert[2], vert[3] };
        Vec3 norm = (vertices[1]-vertices[0]).cross(vertices[2]-vertices[0]);
        norm.normalize();
        Vec3 normals[] = { norm, norm, norm };
        Vec2 tcoords[] = { tc[1], tc[2], tc[3] };
        Material* materials[] = { mat[1], mat[2], mat[3] };

        scene->objects.push_back(
            new Triangle(Vec3::Zero, Quat::Identity, Vec3::Ones,
                         vertices, tcoords, normals, materials, 0));
    }

    {
        Vec3 vertices[] = { vert[2], vert[3], vert[0] };
        Vec3 norm = (vertices[2]-vertices[0]).cross(vertices[1]-vertices[0]);
        norm.normalize();
        Vec3 normals[] = { norm, norm, norm };
        Vec2 tcoords[] = { tc[2], tc[3], tc[0] };
        Material* materials[] = { mat[2], mat[3], mat[0] };

        scene->objects.push_back(
            new Triangle(Vec3::Zero, Quat::Identity, Vec3::Ones,
                         vertices, tcoords, normals, materials, 0));
    }

    {
        Vec3 vertices[] = { vert[3], vert[0], vert[1] };
        Vec3 norm = (vertices[1]-vertices[0]).cross(vertices[2]-vertices[0]);
        norm.normalize();
        Vec3 normals[] = { norm, norm, norm };
        Vec2 tcoords[] = { tc[3], tc[0], tc[1] };
        Material* materials[] = { mat[3], mat[0], mat[1] };

        scene->objects.push_back(
            new Triangle(Vec3::Zero, Quat::Identity, Vec3::Ones,
                         vertices, tcoords, normals, materials, 0));
    }

    for (int i=0; i<4; i++) {

        scene->objects.push_back(
            new Sphere(-vert[i], Quat::Identity, Vec3::Ones, .5, mat[i]));
    }
}

bool ldr_load_staff_scene(Scene* scene, int num)
{
    switch (num)
    {
    case 0:
        ldr_load_scene00(scene);
        break;
    case 1:
        std::cout << "Loaded pool." << std::endl;
        ldr_load_scene01(scene);
        break;
    case 2:
        ldr_load_scene02(scene);
        break;
    default:
        return false;
        break;
    }

    return true;
}

