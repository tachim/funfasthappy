/**
 * @file scene.cpp
 * @brief Function definitions for the Material, Geometry, UpdatableGeometry,
 *  Camera, Light, and Scene classes.
 *
 * @author Eric Butler (edbutler)
 * @author Kristin Siu (kasiu)
 */

/*
   YOU ARE FREE TO MODIFY THIS FILE, as long as you do not change existing
   constructor signatures or remove existing class members. The staff scene
   loader requires all of those to be intact. You may, however, modify
   anything else, including other function signatures and adding additional
   members.
   */

#include "scene.h"
#include "glheaders.h"
#include "imageio.h"
#include <iostream>
#include <geom/sphere.h>
#include <dynlink/cuda_runtime_api_dynlink.h>

using namespace std;

Material::Material():
    diffuse(Vec3::Ones), phong(Vec3::Zero), ambient(Vec3::Ones),
    specular(Vec3::Zero), shininess(0), refraction_index(0),
    texture(0), tex_width(0), tex_height(0)
{
    texture[0] = 0;
}

Material::~Material()
{
    if (on_gpu)
        cudaFree(texture);
    else
        // free the loaded texture, will have no effect if null
        free(texture);
}

void Material::load_texture()
{
    // don't load texture if already loaded or filename is blank
    if (texture_name[0] != 0)
        return;

    std::cout << "loading texture " << texture_name << "...\n";
    texture = imageio_load_image(texture_name,
            &tex_width, &tex_height);
}

Vec3 Material::get_texture_color(const Vec2& tex_coords) const
{
    if (texture) {
        // wrap texture values
        Vec2 tc(tex_coords);
        tc.x = fmod(tc.x, 1);
        tc.y = fmod(tc.y, 1);

        // use nearest sampling to query color
        int tx = static_cast<int>(tc.x * (tex_width - 1));
        int ty = static_cast<int>(tc.y * (tex_height - 1));
        assert(0 <= tx && 0 <= ty && tx < tex_width && ty < tex_height);

        return color_array_to_vector(texture + 4 * (tx + ty * tex_width)).xyz();
    } else
        return Vec3::Ones;
}

void Material::copy_to_gpu(const Material* other)
{
    diffuse = other->diffuse;
    phong = other->phong;
    ambient = other->ambient;
    specular = other->specular;
    shininess = other->shininess;
    refraction_index = other->refraction_index;

    spec_reflec = other->spec_reflec;
    diffuse_reflec = other->diffuse_reflec;
    ambient_reflec = other->ambient_reflec;

    cudaMemcpy(texture_name, other->texture_name, 256, cudaMemcpyHostToDevice);
    tex_height = other->tex_height;
    tex_width = other->tex_width;

    if (cudaMalloc((void **) &texture, tex_width * tex_height) == 
            cudaSuccess)
        cudaMemcpy(texture, other->texture, tex_width * tex_height, cudaMemcpyHostToDevice);
    else
        cerr << "Failed to allocate texture space on device" << endl;
}


Geometry::Geometry():
    position(Vec3::Zero), orientation(Quat::Identity),
    scale(Vec3::Ones) {}

    Geometry::Geometry(const Vec3& pos, const Quat& ori, const Vec3& scl,
            Material* mat, Effect* efc):
        position(pos), orientation(ori), scale(scl), material(mat) {}

        Geometry::~Geometry() {}

Camera::Camera()
    : position(Vec3::Zero), orientation(Quat::Identity), focus_dist(1),
    fov(PI), aspect(1), near_clip(.1), far_clip(10) {}

    const Vec3& Camera::get_position() const
{
    return position;
}

Vec3 Camera::get_direction() const
{
    return orientation * -Vec3::UnitZ;
}

Vec3 Camera::get_up() const
{
    return orientation * Vec3::UnitY;
}

real_t Camera::get_fov_radians() const
{
    return fov;
}

real_t Camera::get_fov_degrees() const
{
    return fov * 180.0 / PI;
}

real_t Camera::get_aspect_ratio() const
{
    return aspect;
}

real_t Camera::get_near_clip() const
{
    return near_clip;
}

real_t Camera::get_far_clip() const
{
    return far_clip;
}

void Camera::translate(const Vec3& v)
{
    position += orientation * v;
}

void Camera::pitch(real_t radians)
{
    rotate(orientation * Vec3::UnitX, radians);
}

void Camera::roll(real_t radians)
{
    rotate(orientation * Vec3::UnitZ, radians);
}

void Camera::yaw(real_t radians)
{
    rotate(orientation * Vec3::UnitY, radians);
}

void Camera::rotate(const Vec3& axis, real_t radians)
{
    orientation = Quat(axis, radians) * orientation;
    orientation.normalize();
}

void Camera::pitch_about_focus(real_t radians)
{
    rotate_about_focus(orientation * Vec3::UnitX, radians);
}

void Camera::yaw_about_focus(real_t radians)
{
    rotate_about_focus(orientation * Vec3::UnitY, radians);
}

void Camera::rotate_about_focus(const Vec3& axis, real_t radians)
{
    // compute rotation, then "swing" camera about focus by that rotation
    Quat rotation(axis, radians);
    Vec3 camdir = orientation * Vec3::UnitZ;
    Vec3 focus = position - (focus_dist * camdir);
    position = focus + ((rotation * camdir) * focus_dist);
    orientation = rotation * orientation;
    orientation.normalize();
}



Light::Light()
    : position(Vec3::Zero), color(Vec3::Ones), intensity(1) {}



Scene::Scene()
    : ambient_light(Vec3::Zero), refraction_index(1),
    caustic_generator(0), n_geom(0),
    n_lights(0), n_materials(0), use_gpu(false)
{
}

void Scene::light_intersections(const Vec3 &origin, const Vec3 &normal, std::vector<const Light *> &output) const
{
    for (int light_ind = 0; light_ind < n_lights; light_ind++)
    {
        const Light *t_light = lights + light_ind;
        Vec3 direction = t_light->position - origin;

        // if the direction is further than 90 degrees from the normal, the light won't affect the object
        // one would think transparent things are counterexamples, but in those cases we calculate the 
        // transport through the object and shoot a ray at the other end
        if (direction.dot(normal) < 0)
            continue;

        Ray to_send;
        to_send.origin = origin;
        to_send.direction = direction;

        real_t t_intersection;
        Geometry *nearest_geom = nearest_intersection(to_send, t_intersection);

        if (nearest_geom == NULL)
            output.push_back(t_light);
    }
}

Geometry* Scene::nearest_intersection(const Ray &ray, real_t &t) const
{
    real_t min_t = 50000000;
    Geometry *ret = NULL;

    real_t t_t;
    for (int i = 0; i < n_geom; i++)
    {
        Geometry *curr_geom = objects + i;
        switch(curr_geom->type)
        {
            case SPHERE:
                t_t = Sphere::intersects(ray, curr_geom);
                break;
            default:
                t_t = -1;
        }
        if (t_t < 0)
            continue;
        if (t_t < min_t)
        {
            min_t = t_t;
            ret = curr_geom;
        }
    }
    t = min_t;
    return ret;
}

void Scene::copy_to_gpu(const Scene &other)
{
    camera = other.camera;
    ambient_light = other.ambient_light;
    refraction_index = other.refraction_index;

    n_lights = other.n_lights;
    n_geom   = other.n_geom;
    n_materials = other.n_materials;

    if (cudaMalloc((void **) &lights, n_lights * sizeof(Light)) != cudaSuccess)
        cerr << "Failed to allocate space for lights on GPU (" << n_lights * sizeof(Light) << ")" << endl;

    if (cudaMalloc((void **) &objects,n_geom * sizeof(Geometry)) != cudaSuccess)
        cerr << "Failed to allocate space for objects on GPU (" <<
            n_geom * sizeof(Geometry) << ")" << endl;

    if (cudaMalloc((void **) &materials, n_materials * sizeof(Material)) != cudaSuccess)
        cerr << "Failed to allocate space for materials on GPU (" <<
            n_materials * sizeof(Material) << ")" << endl;

    for (int i = 0; i < n_geom; i++)
    {
        Geometry *curr_geom = objects + i;
        const Geometry *other_geom = other.objects + i;

        curr_geom->material = other_geom->material - other.materials + materials;

        int size;
        switch (curr_geom->type)
        {
            case SPHERE:
                size = sizeof(SphereData);
                break;
            default:
                cerr << "Trying to copy unknown geom type" << endl;
        }
        if (cudaMalloc((void **) &curr_geom->geom_specific, size) != cudaSuccess)
            cerr << "Failed to allocate room for specific geom data" << endl;
        if (cudaMemcpy(curr_geom->geom_specific, 
                    other_geom->geom_specific,
                    size,
                    cudaMemcpyHostToDevice) != cudaSuccess)
            cerr << "Failed to copy specific geometry data" << endl;
    }

    for (int i = 0 ; i < n_materials; i++)
    {
        Material *curr_material = materials + i;
        const Material *other_material = other.materials + i;

        curr_material->copy_to_gpu(other_material);
    }
}

Material* Scene::alloc_material()
{
    if (use_gpu == true)
    {
        cerr << "Trying to add material to scene after copied to GPU. This is wrong." << endl;
        return NULL;
    }
    Material *ret;
    if (n_materials == 0)
    {
        materials = (Material *) malloc(sizeof(Material));
        ret = materials;
    }
    else
    {
        materials = (Material *) realloc(materials, (n_materials + 1) * sizeof(Material));
        ret = materials + n_materials;
    }
    n_materials++;
    ret->init();
    return ret;
}

Geometry* Scene::alloc_geom()
{
    if (use_gpu == true)
    {
        cerr << "Trying to add geometry to scene after copied to GPU. This is incorrect behavior." << endl;
        return NULL;
    }
    Geometry *to_add;
    if (n_geom == 0)
    {
        objects = (Geometry *) malloc(sizeof(Geometry));
        to_add = objects;
    }
    else
    {
        objects = (Geometry *) realloc(objects, (n_geom + 1) * sizeof(Geometry));
        to_add = objects + n_geom;
    }
    n_geom++;
    return to_add;
}

Light* Scene::alloc_light()
{
    if (use_gpu == true)
    {
        cerr << "Trying to add light after copied to GPU. Wrong." << endl;
        return NULL;
    }
    Light *ret;
    if (n_lights == 0)
    {
        lights = (Light *) malloc(sizeof(Light));
        ret = lights;
    }
    else
    {
        lights = (Light *) realloc(lights, (n_lights + 1) * sizeof(Light));
        ret = lights + n_lights;
    }
    n_lights++;
    return ret;
}
