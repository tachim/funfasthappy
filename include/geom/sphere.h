/**
 * @file sphere.h
 * @brief Class defnition for Sphere.
 *
 * @author Kristin Siu (kasiu)
 * @author Eric Butler (edbutler)
 */

/*
    EDIT THIS FILE FOR P1. However, do not change existing constructor
    signatures. The staff scene loader requires all of those to be intact.
 */

#ifndef _SPHERE_H_
#define _SPHERE_H_

#include "scene.h"

struct SphereData
{
    real_t radius;
};

class Sphere
{
public:
    static float intersects(const Ray &ray, const Geometry *geom);
    static void  get_normal(const Vec3 &pt, const Geometry *geom, Vec3& out);
};

#endif

