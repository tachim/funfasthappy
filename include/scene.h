/**
 * @file scene.h
 * @brief Class definitions of Material, Geometry, UpdatableGeometry,
 *  Camera, Light, and Scene.
 *
 * @author Eric Butler (edbutler)
 * @author Kristin Siu (kasiu)
 */

/*
   YOU ARE FREE TO MODIFY THIS FILE, as long as you do not change existing
   constructor signatures or remove existing class members. The staff scene
   loader requires all of those to be intact. You may, however, modify
   anything else, including other function signatures and adding additional
   members.
   */

#ifndef _SCENE_H_
#define _SCENE_H_

#include "effect.h"
#include "vec/vec.h"
#include "vec/quat.h"
#include <string>
#include <vector>
#include <dynlink/cuda_runtime_api_dynlink.h>

struct Ray
{
    Vec3 origin, direction;
};

enum {
    SPHERE,
    TRIANGLE
};

/**
 * Represents a material property for a geometry or part of a geometry.
 */
class Material
{
    public:
        /**
         * P1 NOTE: the only items that must be dealt with are diffuse, phong,
         * ambient, and shininess. These correspond to diffuse, specular,
         * ambient, and shininess opengl material properties.
         */

        // the diffuse color
        Vec3        diffuse;
        // the phong specular color
        Vec3        phong;
        // the ambient color
        Vec3        ambient;
        // the specular reflection color (raytrace only)
        Vec3        specular;
        // the phong shininess
        real_t      shininess;
        // refractive index of material dielectric; 0 is special case for
        // infinity, i.e. opaque (raytrace only)
        real_t      refraction_index;

        real_t      spec_reflec, diffuse_reflec, ambient_reflec;

        // filename of the texture
        char        texture_name[256];
        // pointer to texture array
        unsigned char* texture;
        // dimensions of texture
        int         tex_width, tex_height;

        bool        on_gpu;

        Material();

        void init()
        {
            tex_width = 0;
            tex_height = 0;
            on_gpu = false;
        }

        ~Material();

        /**
         * Returns true iff this is opaque (no refraction)
         */
        bool is_opaque() const
        {
            return refraction_index == 0;
        }

        /**
         * Loads the texture to a buffer.
         */
        void load_texture();

        /**
         * Returns the texture color at the given s,t texture coordinates.
         * The values are wrapped to [0,1).
         */
        Vec3 get_texture_color(const Vec2& tex_coords) const;

        void copy_to_gpu(const Material *other);
    private:
        // no meaningful assignment or copy
        Material(const Material&);
        Material& operator=(const Material& other);
};

class Geometry
{
    public:
        Geometry();
        Geometry(const Vec3& pos, const Quat& ori, const Vec3& scl,
                Material* mat, Effect* efc);
        virtual ~Geometry();

        /*
           World transformation are applied in the following order:
           1. Scale
           2. Orientation
           3. Position
           */

        // the type of geometry, since CUDA doesn't support virtual functions.
        int type;

        // a pointer to a struct containing info specific to the <type>
        void *geom_specific;

        // The world position of the object.
        Vec3 position;

        // The world orientation of the object.
        Quat orientation;

        // The inverse of the world orientation
        Quat inverse_orientation;

        // The world scale of the object.
        Vec3 scale;
        // The material to use to render this object in opengl (raytracing may use
        // a different material.
        Material* material;

        /**
         * Renders this geometry using OpenGL in the local coordinate space.
         */
        real_t intersects(const Ray &ray) const;
        void   get_normal(const Vec3 &pt, Vec3& out);

        Geometry& operator=(const Geometry& other);
};

class UpdatableGeometry : public Geometry
{
    public:
        UpdatableGeometry() {}
        UpdatableGeometry(const Vec3& pos, const Quat& ori, const Vec3& scl,
                Material* mat, Effect* efc)
            : Geometry(pos, ori, scl, mat, efc) {}
        virtual ~UpdatableGeometry() {}
        /**
         * Updates this Geometry to the given time.
         * @param time The absolute world time.
         */
        virtual void update(real_t time) = 0;
};

/**
 * Stores position data of the camera.
 */
class Camera
{
    public:
        Camera();

        // accessor functions

        // Returns the world position.
        const Vec3& get_position() const;
        // Returns the direction vector, a unit vector pointing in the direction
        // the camera is facing.
        Vec3 get_direction() const;
        // Returns the up vector, a unit vector pointing in the direction up from
        // the camera's orientation.
        Vec3 get_up() const;
        // Returns the field of view in radians.
        real_t get_fov_radians() const;
        // Returns the field of view in degrees.
        real_t get_fov_degrees() const;
        // Returns the aspect ratio (width/height).
        real_t get_aspect_ratio() const;
        // Returns the distance from the camera to the near clipping plane.
        real_t get_near_clip() const;
        // Returns the distance from the camera to the far clipping plane.
        real_t get_far_clip() const;

        // mutator functions

        // translates position by v
        void translate(const Vec3& v);
        // rotates about the X axis
        void pitch(real_t radians);
        // rotates about the Z axis
        void roll(real_t radians);
        // rotates about the Y axis
        void yaw(real_t radians);
        // rotates about the given axis
        void rotate(const Vec3& axis, real_t radians);
        // swings camera around focal point along X axis
        void pitch_about_focus(real_t radians);
        // swings camera around focal point along Y axis
        void yaw_about_focus(real_t radians);
        // swings camera around focal point along given axis
        void rotate_about_focus(const Vec3& axis, real_t radians);

        // members

        // The world position of the camera.
        Vec3 position;
        // The orientation of the camera, relative to a default direction
        // of negative z axis and default up vector of y axis.
        Quat orientation;
        // Distance to the point about which the camera's rotate functions operate.
        real_t focus_dist;
        // Field of view of y-axis, in radians.
        real_t fov;
        // The aspect ratio.
        real_t aspect;
        // The near clipping plane.
        real_t near_clip;
        // The far clipping plane.
        real_t far_clip;
};

class Light
{
    public:
        Light();

        // The position of the light, relative to world origin.
        Vec3 position;
        // The color of the light (both diffuse and specular)
        Vec3 color;
        // Total intensity of this light. (USED STARTING P4)
        real_t intensity;
};

/**
 * The container class for information used to render a scene composed of
 * Geometries.
 */
class Scene
{
    bool use_gpu;
    // list of all lights in the scene
    int     n_lights;
    Light   *lights;
    // list of all objects (includes updatable objects). deleted in dctor
    int      n_geom;
    Geometry *objects;
    // list of all materials used by the objects/effects. deleted in dctor
    int      n_materials;
    Material *materials;

    // the geometry in the scene that should be fired at to generate
    // caustics (USED STARTING P4)
    Geometry* caustic_generator;

    public:
    // the camera
    Camera camera;
    // the amibient light of the scene
    Vec3 ambient_light;
    // the refraction index of air (USED STARTING P3)
    real_t refraction_index;

    Geometry* alloc_geom();
    Material* alloc_material();
    Light*    alloc_light();

    Geometry* nearest_intersection(const Ray &e, real_t &t) const;
    void      light_intersections(const Vec3 &origin, const Vec3 &normal, std::vector<const Light *> &out) const;

    void copy_to_gpu(const Scene &other);
    /**
     * Creates a new empty scene.
     */
    Scene();

    /**
     * Destroys this scene. Deletes all objects in materials and objects.
     * Assumes that all objects in updatable_objects are also in objects.
     */
    ~Scene()
    {
        if (n_lights > 0)
            if (use_gpu)
                cudaFree(lights);
            else
                free(lights);
        for (int i = 0; i < n_materials; i++)
        {
            Material *curr = materials + i;
            if (use_gpu)
                cudaFree(curr->texture);
            else
                free(curr->texture);
        }
        if (n_materials > 0)
            if (use_gpu)
                cudaFree(materials);
            else
                free(materials);
        for (int i = 0; i < n_geom; i++)
        {
            Geometry *curr_geom = objects + i;
            if (use_gpu)
                cudaFree(curr_geom->geom_specific);
            else
                free(curr_geom->geom_specific);
        }
        if (n_geom > 0)
            if (use_gpu)
                cudaFree(objects);
            else
                free(objects);
    };

    private:
    // used to copy memory to graphics card shared mem
    Scene(const Scene&);
};
#endif /* _SCENE_H_ */
